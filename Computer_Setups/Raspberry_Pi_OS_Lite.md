# Raspberry Pi OS Lite

__Last Updated:__ 2023-01-15

[[_TOC_]]

## General Notes

* As of this writing, Raspberry Pi OS is based on Debian 11.

## Create the SD Card

### Get Raspberry Pi OS Lite Image

* Download Raspberry Pi OS Lite from [here](https://www.raspberrypi.com/software/operating-systems/).
  * These instructions use the downloaded file name of: *2022-09-22-raspios-bullseye-arm64-lite.img.xz*
* Verify the checksum against what is on the website:
  ```bash
  sha256sum 2022-09-22-raspios-bullseye-arm64-lite.img.xz
  ```
* Un-archive the image:
  ```bash
  unxz 2022-09-22-raspios-bullseye-arm64-lite.img.xz
  ```

### Write Raspberry Pi OS Lite Image to SD Card

* Insert your SD card (do not mount it).
* Find the device (disk) address of your SD card:
  ```bash
  lsblk|head -1;lsblk|grep " disk"

  # Example Output
  #   NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINTS
  #   sda      8:0    0 953.9G  0 disk 
  #   sdb      8:16   1    29G  0 disk     <-- This is the disk I want, it's address is /dev/sdb 
  #   sdc      8:32   1     0B  0 disk 
  #   sdd      8:48   1     0B  0 disk 
  #   sde      8:64   1     0B  0 disk
  ```
* Write the disk image to the SD card. After the write is complete, perform a sync to flush out the cache.
  This will take some time:
  ```bash
  # if = input file, of = output file (disk device address), bs = block size
  
  sudo dd if=2022-09-22-raspios-bullseye-arm64-lite.img of=/dev/sdb bs=4096
  sudo sync
  ```

### Configure Raspberry PI OS Lite Before First Use

* Mount the boot partition of the SD card:
  ```bash
  sudo mount /dev/sdb1 /mnt/temp
  ```
* To enable ssh, create an empty ssh file
  ```bash
  cd /mnt/temp
  sudo touch ssh
  ```
* Unmount the boot partition of the SD card:
  ```bash
  cd
  sudo umount /mnt/temp 
  ``` 

### Final Steps

* Remove the SD card and insert it in to the Raspberry Pi.

## First Boot

* The first boot of Raspberry Pi OS will take a little extra time, as it will resize itself to fit all the available
  space on the SD card, and perform a reboot.
* You need to be hooked in to a monitor with keyboard, so you can go through the interactive prompts of setting up
  the user, locale, etc.
* Login as the user you set up.
* Update the system:
  ```bash
  sudo apt-get update
  sudo apt-get upgrade --yes
  sudo reboot
  ```
* Use raspi-config to configure the Raspberry Pi:
  ```bash
  sudo raspi-config
  
  # Depending on what you change / select, raspi-config may perform a reboot for you.
  sudo reboot
  ```
