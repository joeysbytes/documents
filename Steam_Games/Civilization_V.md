# Civilization V

This game will not start if you have more than 8 cores.  This can be fixed by adding the following line to the launch
options:

```text
LD_PRELOAD=/usr/lib32/libopenal.so.1 taskset -c 0-6 %command%
```

[Source](https://steamcommunity.com/app/8930/discussions/0/1693788384127278334/?ctp=3)
