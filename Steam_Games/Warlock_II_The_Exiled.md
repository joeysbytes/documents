# Warlock II: The Exiled

This game is linux native, but does not handle multi-screen well.

For optimal gameplay, disable all but your primary monitor, then start the game.

If you want to keep your second monitor on, you can add these launch options:

```text
-screen-fullscreen 0 -screen-width 1920 -screen-height 1080
```

The opening movie scenes, and the title screen, do not look correct, but once you start the game, it is very 
playable (though still doesn't look quite right).
